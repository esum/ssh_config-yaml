#!/usr/bin/python3

import argparse
import itertools
import os
import sys
import yaml


CONFIG_FILE = os.getenv('HOME') + '/.ssh/config.yaml'

def get_config(file):
	config = yaml.load(file, Loader=yaml.FullLoader)
	stop = False
	while not stop:
		stop = True
		for include in range(len(config) - 1, -1, -1):
			if 'include' in config[include]:
				stop = False
				file_path = os.path.expandvars(rc[include]['include'])
				with open(file_path) as file:
					config[include:include + 1] = yaml.load(file, Loader=yaml.FullLoader)
	return config

def iterable_builder(in_block):
	if isinstance(in_block, dict):
		for keyword in in_block:
			if keyword == 'zip':
				return zip(*[iterable_builder(block) for block in in_block[keyword]])
			elif keyword == 'chain':
				return itertools.chain(*[iterable_builder(block) for block in in_block[keyword]])
			elif keyword == 'product':
				return itertools.product(*[iterable_builder(block) for block in in_block[keyword]])
	elif isinstance(in_block, list):
		return in_block

def match_host_builder(block, file, iterables=[()], keys=[]):
	for iterable in iterables:
		if 'Match' in block:
			file.write('Match')
			for keyword in block['Match']:
				file.write(f''' {keyword} "{','.join(block['Match'][keyword])}"''')
			file.write('\n')
		elif 'Host' in block:
			file.write('Host ')
			if isinstance(block['Host'], list):
				file.write(' '.join(host.format(**{key: iterable[i] for i, key in enumerate(keys)}) for host in block['Host']))
			elif isinstance(block['Host'], str):
				file.write(block['Host'].format(**{key: iterable[i] for i, key in enumerate(keys)}))
			file.write('\n')
		for keyword in block:
			if keyword in {'Match', 'Host'}:
				continue
			file.write(f'\t{keyword} {block[keyword]}\n'.format(**{key: iterable[i] for i, key in enumerate(keys)}))
		file.write('\n')

def for_builder(for_block, file):
	if 'in' not in for_block:
		return {}
	if 'as' not in for_block:
		return {}
	if 'do' not in for_block:
		return {}
	iterable = iterable_builder(for_block['in'])
	do = match_host_builder(for_block['do'], file, iterable, for_block['as'])

def to_ssh(config, file):
	config_defines = {}
	for block in config:
		if 'comment' in block:
			file.write(f'''# {block['comment']}\n''')
		elif 'for' in block:
			for_builder(block['for'], file)
		elif 'Match' in block or 'Host' in block:
			match_host_builder(block, file)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="Build SSH config file from a YAML config file.")
	parser.add_argument('-i', '--infile', metavar='FILE', help="input file", type=argparse.FileType(), default=None)
	args = parser.parse_args()
	if args.infile is None:
		try:
			file = open(CONFIG_FILE)
		except FileNotFoundError:
			print("You must either specify a file with --infile or create a ~/.ssh/config.yaml file.", file=sys.stderr)
			sys.exit(1)
	else:
		file = args.infile
	config = get_config(file)
	to_ssh(config, sys.stdout)
