# ssh_config-yaml

A YAML interpreter for building SSH config file

## Example configuration

`~/.ssh/config.yml`

```yaml
- Host: server0.crans.org
  User: user

- for:
    in: 
      zip:
        - [   server1,    server2,    server3]
        - [172.16.0.1, 172.16.0.2, 172.16.0.3]
    as:
      - host
      - ip
    do:
      Host: ["{host}", "{host}.crans.org", "{ip}"]
      User: user
      IdentityFile: ~/.ssh/key
```

Will generate :

```
Host server0.crans.org
	User user

Host server1 server1.crans.org 172.16.0.1
	User user
	IdentityFile ~/.ssh/key

Host server2 server2.crans.org 172.16.0.2
	User user
	IdentityFile ~/.ssh/key

Host server3 server3.crans.org 172.16.0.3
	User user
	IdentityFile ~/.ssh/key
```
